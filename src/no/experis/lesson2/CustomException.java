package no.experis.lesson2;

//Class that defines a CustomException using Exception as its super

public class CustomException extends Exception {

    //Constructor for CustomException using its super (Exception) constructor
    public CustomException(String message) {
        super(message);
    }
}
