package no.experis.lesson2;

//Class that lets you divide two numbers against each other, and checks for divide by 0 error with
//a custom exception and a standard ArithemicException handeling the error

public class ExceptionExercise {

    //Divide method implementing a standard ArithmeticException
    public static double divideDouble(double number1, double number2) {

        //Checks if the number2 parameter is 0, and throws a ArithmeticException if that is the case
        if (number2 == 0) {
            throw new ArithmeticException("You cant divide by zero");
        }
        return number1 / number2;
    }

    //Divide method implementing the CustomException
    public static double divideDoubleWithCustomException(double number1, double number2) throws CustomException {

        //Checks if the number2 parameter is 0, and throws a CustomException if that is the case
        if (number2 == 0)  {
            throw new CustomException("My custom exception");
        }
        return number1 / number2;
    }


    public static void main(String[] args) {

        //Try-catch block with ArithmeticException
        try {
            System.out.println(ExceptionExercise.divideDouble(3,0));
        } catch (ArithmeticException e) {
            System.out.println(e);
        }

        //Try-catch block with CustomException
        try {
            System.out.println(ExceptionExercise.divideDoubleWithCustomException(3,0));
        } catch (CustomException e) {
            System.out.println(e);
        }

    }

}
